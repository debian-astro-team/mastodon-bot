# mastodon-bot

This repository contains a simple bot that can be used to post a
random package description every week to the
@DebianAstro@astrodon.social account, which is operated by
olebole@debian.org

It requires a token from the account to be set as MASTODON_TOKEN
environment variable.

Required packages:
 * python3
 * python3-mastodon
 * python3-apt
 